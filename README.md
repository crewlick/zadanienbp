**# Instrukcja uruchomienia: #**


W konsoli otwieramy katalog target.
Przed uruchomieniem programu należy uruchomić plik wsadowy **setPath.bat**

** Następnie uruchamiamy program poleceniem: **

java pl.parser.nbp.MainClass [KOD WALUTY][DATA POCZĄTKOWA][DATA KOŃCOWA]

**[KOD WALUTY]** - trzyliterowy kod - USD, AUD, CAD, EUR, HUF, GBP, JPY, CZK, DKK, NOK, SEK


**[DATA POCZĄTKOWA] [DATA KOŃCOWA]**- data w formacie YYYY-MM-DD

Najlepszym rozwiązaniem dla tego programu byłoby oczywiście skorzystanie z API które na podstawie odpowiedniego zapytania jest w stanie zwrócić tylko interesujące nas dane.

Program najpierw pobiera pliki dir, wyszukuje w nich odpowiednich nazw plików xml a następnie pobiera tylko niezbędne pliki.

Zdecydowałem się na zapisywanie plików na dysku zamiast parsowania danych prosto ze streamu.
Ten sposób pozwala na maksymalne ograniczenie zapytań do nbp, a program pozwala na dokonywanie obliczeń na podstawie zapisanych już danych w przypadku braku połączenia z internetem.