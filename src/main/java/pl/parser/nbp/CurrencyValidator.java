package pl.parser.nbp;

public class CurrencyValidator {
	public static final String CURRENCIES = "USD,AUD,CAD,EUR,HUF,GBP,JPY,CZK,DKK,NOK,SEK";

	public static final void checkCurrencyFormat(String currency) { // sprawdzenie czy kod waluty jest poprawny

		if (!CURRENCIES.contains(currency) || currency.contains(",") || currency.length() != 3) {
			throw new IllegalArgumentException("Niepoprawny format waluty prosz� u�y� jednego z nast�puj�cych format�w USD, AUD, CAD, EUR, HUF, GBP, JPY, CZK, DKK, NOK, SEK");
		}

	}
}
