package pl.parser.nbp;

import java.util.ArrayList;
import java.util.List;

public class Calculator {
	private List<Currency> currencyList = new ArrayList<Currency>();

	public Calculator(List<Currency> currencyList) {
		super();
		this.currencyList = currencyList;
	}

	public double calculateAverageBuyPrice() {
		double mean = 0;
		for (Currency val : currencyList) {
			mean += val.getBuyPrice();
		}
		mean = mean / currencyList.size();
		return mean;
	}

	public double calculateStandardDeviation() {
		double mean = 0;
		for (Currency val : currencyList) {
			mean += val.getSellPrice();
		}
		mean = mean / currencyList.size();
		double diff = 0;
		for (Currency val : currencyList) {
			diff += Math.pow(val.getSellPrice() - mean, 2);
		}
		double diffMean = diff / currencyList.size();

		return Math.sqrt(diffMean);

	}
}
