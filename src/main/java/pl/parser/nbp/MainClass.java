package pl.parser.nbp;

import java.io.File;

import java.util.List;

public class MainClass {

	public static void main(String[] args) {

		try {
			String currencyCode = args[0];
			String startDate = args[1];
			String endDate = args[2];

			CurrencyValidator.checkCurrencyFormat(currencyCode); // Walidacja poprawno�ci formatu waluty

			List<File> files = NbpService.getXmlFiles(startDate, endDate); // pobieranie plik�w xml

			List<Currency> values = XmlParseUtil.getXmlObjects(currencyCode, files); // pobiera list� kurs�w kupna i sprzeda�y z wybranego zakresu dat

			Calculator calc = new Calculator(values);
			Double average = calc.calculateAverageBuyPrice(); // obliczenie �redniej
			Double deviation = calc.calculateStandardDeviation(); // obliczenie odchylenia

			System.out.printf("�redni kurs kupna waluty " + currencyCode + ": %.4f\n", average);
			System.out.printf("Odchylenie standartowe kursu sprzeda�y waluty " + currencyCode + ": %.4f\n", deviation);

		} catch (IndexOutOfBoundsException e) {

			System.out.println("Niepoprawne parametry wej�ciowe");

		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
		} catch (NoClassDefFoundError e) {
			System.out.printf("Uruchom plik setPath.bat a nast�pnie uruchom program z poziomu katologu target");
		}

	}

}
