package pl.parser.nbp;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XmlParseUtil {

	private static final String DATE_EXPRESSION = "/tabela_kursow/data_publikacji"; // wyra�enie okre�laj�ce pole data_publikacji w Xpath
	private static final String EXPRESSION_OBJECT_PARENT = "/tabela_kursow/pozycja/"; // wyra�enie okre�laj�ce rodzica Xpath
	private static final String EXPRESION_CURRENCY_CODE = "[../kod_waluty/text()='"; // cz�� wyra�enia dzi�ki kt�remu pobieramy warto�� pola na podstawie kodu waluty XPath
	private static final String EXPRESSION_END = "']"; // zako�czenie wyra�enia Xpath
	public static final String DECIMAL_SEPARATOR = ","; // separator dziesi�tny

	private XmlParseUtil() {
	}

	public static List<Currency> getXmlObjects(String code, List<File> files) { // pobieranie Listy obiekt�w Currency z zapisanych plik�w XML na podstawei kodu waluty i listy plik�w xml

		XPath xPath = XPathFactory.newInstance().newXPath();
		List<Currency> currencyList = new ArrayList<Currency>();
		DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;

		try {
			builder = builderFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}

		for (File file : files) {
			try {
				String date = getDate(builder, file, xPath); // pobieranie daty
				double buy = getFieldValue("kurs_kupna", code, builder, file, xPath);
				double sell = getFieldValue("kurs_sprzedazy", code, builder, file, xPath); // pobieranie kurs�w kupna i sprzeda�y dla odpowiedniej waluty
				Currency currency = new Currency(date, buy, sell);
				currencyList.add(currency);
			} catch (NoSuchElementException e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}

		return currencyList;
	}

	private static double getFieldValue(String fieldName, String code, DocumentBuilder builder, File file, XPath xPath) // Pobieranie warto��i z wskazanego pliku XML dla wybranego pola oraz kodu waluty
			throws NoSuchElementException {

		try {
			Document document = builder.parse(new FileInputStream(file));
			String expression = EXPRESSION_OBJECT_PARENT + fieldName + EXPRESION_CURRENCY_CODE + code + EXPRESSION_END; // tworzenie zapytania xPath

			NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(document, XPathConstants.NODESET); // parsowanie odpowiendich "w�z��w" xml
			for (int i = 0; i < nodeList.getLength(); i++) {
				String nodeValue = nodeList.item(i).getFirstChild().getNodeValue();
				nodeValue = nodeValue.replaceAll(DECIMAL_SEPARATOR, ".");
				return Double.valueOf(nodeValue);
			}
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		throw new NoSuchElementException("Nie mo�na pobra� warto�ci pola" + fieldName + "w pliku" + file.getName()); // rzucenie wyj�tku w przypadku braku danych

	}

	private static String getDate(DocumentBuilder builder, File file, XPath xPath) // Pobieranie daty (niepotrzebne do dzia�ania progamu ale by�o w wymaganiach)
			throws NoSuchElementException {

		try {
			Document document = builder.parse(new FileInputStream(file));
			String expression = DATE_EXPRESSION; // tworzenie zapytania xPath
			NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(document, XPathConstants.NODESET); // parsowanie odpowiendich "w�z��w" xml
			for (int i = 0; i < nodeList.getLength(); i++) {
				return nodeList.item(i).getFirstChild().getNodeValue();
			}
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		throw new NoSuchElementException("Nie mo�na pobra� warto�ci daty"); // rzucenie wyj�tku w przypadku braku daty

	}
}
