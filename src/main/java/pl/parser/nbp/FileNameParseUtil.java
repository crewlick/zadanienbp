package pl.parser.nbp;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.Set;

public final class FileNameParseUtil {
	private final static String TABLE_TYPE = "c"; // Typ tabeli NBP
	private final static String EXTENSION = ".xml"; // Rozszerzenie plik�w
	private final static String ENCODING = "UTF-8"; // Kodowanie pliku textowego
	private final static String INPUT_FORMAT = "yyyy-MM-dd"; // format daty w parametrach wejsciowych
	private final static String OUTPUT_FORMAT = "yyMMdd"; // format daty w nazwie pliku xml
	private final static String YEAR_FORMAT = "yyyy"; // format daty w nazwach plik�w dir

	private FileNameParseUtil() {
	}; // prywatny konstruktor (aby nie tworzy� instancji klay typu Util)

	public static List<String> getFileNamesFromDirFiles(String startDate, String endDate, List<File> txtFiles) throws NoSuchElementException {
		List<String> fileNames = new ArrayList<String>();
		try {
			List<String> dates = getDaysBetweenDates(startDate, endDate); // pobieranie daty wszytkie daty pomi�dzy pocz�tkiem i ko�cem
			fileNames = scanForFileNames(dates, txtFiles); // szukanie dat w plikach dir i tworzenie nazwy plik�w xml do pobrania

		} catch (DateTimeException e) {
			System.out.println(e.getLocalizedMessage());
			System.exit(0);
		}
		if (fileNames.isEmpty()) {
			throw new NoSuchElementException("Brak Notowa� w podanym przedziale czasu");
		}
		return fileNames;

	}

	private static List<String> scanForFileNames(List<String> dates, List<File> txtFiles) { // zwraca list� nazw plik�w xml kt�re nale�y pobra� aby uzyska� dane potrzebne do oblicze�

		List<String> discoveredNames = new ArrayList<String>();
		for (File txtFile : txtFiles) {
			for (String date : dates) {
				try {
					Scanner scanner = new Scanner(txtFile, ENCODING);
					// tutaj mo�na byo parsowa� text bezpo�rednio z adresu URL przez URL.openStream()
					// zapisanie plik�w na dysku przyspieszy dzialanie programu po pierwszym odpaleniu eliminuj�c zb�dne zapytania
					// i pozwoli na p�niejsze dzialanie programu bez po��czenia z internetem na podstawie zapisanych danych.
					int lineNum = 0;
					while (scanner.hasNextLine()) {
						String line = scanner.nextLine();
						lineNum++;
						if (line.contains(TABLE_TYPE) && line.contains(date)) {// dodanie linii zawieraj�cej typ tabeli (c) oraz odpowiedni� dat�
							discoveredNames.add(line.substring(line.indexOf(TABLE_TYPE)) + EXTENSION); // stworzenie pe�nej nazwy pliku xml i dodanie do listy
						}
					}
				} catch (FileNotFoundException e) {
					System.out.println("nie mo�na znale�� pliku " + txtFile.getName());
					System.exit(0);
					
				}
			}
		}

		return discoveredNames;
	}

	private static List<String> getDaysBetweenDates(String startDate, String endDate) throws DateTimeException { // zwraca list� dat w formacie umo�liwiaj�cym przeszukiwanie plik�w dir

		DateFormat inputFormat = new SimpleDateFormat(INPUT_FORMAT, Locale.getDefault());
		inputFormat.setLenient(false);
		DateFormat outputFormat = new SimpleDateFormat(OUTPUT_FORMAT, Locale.getDefault());

		Date start = formatDate(startDate);
		Date end = formatDate(endDate);

		List<String> dates = new ArrayList<String>();
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(start);
		if (end.before(start)) {
			throw new DateTimeException("Data ko�cowa nie mo�eby� wcze�niejsza od pocz�tkowej");
		}
		while (calendar.getTime().before(end) || calendar.getTime().equals(end)) {
			Date result = calendar.getTime();
			String dateString = outputFormat.format(result).toString();
			dates.add(dateString);
			calendar.add(Calendar.DATE, 1);
		}

		return dates;
	}

	public static List<String> getYearsBeteweenDates(String startDate, String endDate) { // zwraca list� lat mi�dzy dat� pocz�tkow� i ko�cow� w celu pobrania odpowienich plik�w dir

		DateFormat outputFormat = new SimpleDateFormat(YEAR_FORMAT, Locale.getDefault());

		Date start = formatDate(startDate);
		Date end = formatDate(endDate);

		List<String> years = new ArrayList<String>();
		int startYear = Integer.valueOf(outputFormat.format(start).toString());
		int endYear = Integer.valueOf(outputFormat.format(end).toString());

		for (int year = startYear; year <= endYear; year++) {
			if (year != Calendar.getInstance().get(Calendar.YEAR)) {
				years.add(String.valueOf(year));
			} else {
				years.add(""); // plik z obecnego roku nie nosi nazwy dir[rok].txt tylko dir.txt wi�c dodajemy pusty string
			}
		}

		return years;

	}

	private static Date formatDate(String date) { // parsowanie Stringa do Date w odpowiednim formacie
		DateFormat inputFormat = new SimpleDateFormat(INPUT_FORMAT, Locale.getDefault());
		inputFormat.setLenient(false);
		Date formattedDate = null;
		try {
			formattedDate = inputFormat.parse(date);
		} catch (ParseException e) {
			System.out.println("Niepoprawny format daty");
			System.exit(0);
		}
		return formattedDate;
	}

}
