package pl.parser.nbp;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.apache.commons.io.FileUtils;

public class NbpService { // Service pobieraj�cy pliki z serwer�w Nbp

	private static NbpService nbpService = new NbpService();

	private final static String URL = "http://www.nbp.pl/kursy/xml/"; // url do lokalizacji z plikami xml;
	private final static String DIR_FILE_NAME = "dir"; // nazwa pliku zawieraj�cego nazwy plik�w xml
	private final static String DIR_FILE_EXTENSION = ".txt"; // rozszerzenie pliku dir

	private static List<File> getDirFiles(String startDate, String endDate) { // Pobieranie plik�w dir

		List<File> dirFiles = new ArrayList<File>();
		List<String> years = FileNameParseUtil.getYearsBeteweenDates(startDate, endDate);

		for (String year : years) {
			String fileName = DIR_FILE_NAME + year + DIR_FILE_EXTENSION;
			String url = URL + fileName;

			try {
				URL dirFileUrl = new URL(url);
				dirFiles.add(getFile(dirFileUrl, fileName));
			} catch (MalformedURLException e1) {
				System.out.println("Niepoprawny adres url: " + url);			
			}

		}
		return dirFiles;
	}

	public static List<File> getXmlFiles(String startDate, String endDate) { // pobieranie plik�w XML

		List<File> xmlFiles = new ArrayList<File>();
		List<File> dirFiles = getDirFiles(startDate, endDate); // pobranie nazw niezb�dnych plik�w dir
		try {
			List<String> xmlFileNames = FileNameParseUtil.getFileNamesFromDirFiles(startDate, endDate, dirFiles); // pobranie nazw plik�w xml z plik�w dir

			for (String xmlFileName : xmlFileNames) {
				String url = URL + xmlFileName;
				URL xmlFileUrl;
				try {
					xmlFileUrl = new URL(url);
					xmlFiles.add(getFile(xmlFileUrl, xmlFileName));
				} catch (MalformedURLException e) {
					System.out.println("Niepoprawny adres url: " + url);
				}

			}
		} catch (NoSuchElementException e) {
			System.out.println(e.getMessage());
			System.exit(0);
		}
		return xmlFiles;

	}

	private static File getFile(URL url, String fileName) { // pobieranie plik�w
		File file = new File(fileName);
		if (!file.exists() || fileName.equals(DIR_FILE_NAME + DIR_FILE_EXTENSION)) { // Pliki kt�re zostay ju� pobrane nie s� ponownie pobierane poza plikem "dir.txt" kt�ry jest codziennie aktualizowany przez NBP
			try {
				FileUtils.copyURLToFile(url, file);
			} catch (IOException e) {
				System.out.println("Nie mo�na pobra� pliku " + url);
				System.out.println("Obliczenia mog� zosta� dokonane na podstawie nieaktualnych dancyh !!!");
				System.out.println("Sprawd� po��czenie z Internetem");
			}
		}
		return file;
	}

}
