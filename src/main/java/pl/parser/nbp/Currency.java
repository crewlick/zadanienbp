package pl.parser.nbp;

public class Currency {
	public String date; // data_publikacji (nigdzie si� nie przyda�a poniewa� data publikacji zakodowana jest te� w nazwie pliku xml, by�a w wymaganiach wi�c jest pobierana)
	public double buyPrice; // kurs_kupna
	public double sellPrice; // kurs_sprzedarzy

	public Currency(String date, double buyPrice, double sellPrice) {
		super();
		this.date = date;
		this.buyPrice = buyPrice;
		this.sellPrice = sellPrice;
	}

	public double getBuyPrice() {
		return buyPrice;
	}

	public void setBuyPrice(double buyPrice) {
		this.buyPrice = buyPrice;
	}

	public double getSellPrice() {
		return sellPrice;
	}

	public void setSellPrice(double sellPrice) {
		this.sellPrice = sellPrice;
	}

}
